Bootstrap: library
From: ubuntu:20.04

%labels
    Author fabian.rost@tu-dresden.de
    Organisation DcGC

%environment
  export DEBIAN_FRONTEND=noninteractive
  export PATH=/opt/conda/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
  export NUMBA_CACHE_DIR=/tmp

%post
  set -eu

  # Build date (when building, pass via SINGULARITYENV_CONTAINER_BUILD_DATE otherwise date when building)
  if [ -z ${CONTAINER_BUILD_DATE+x} ]
  then
    CONTAINER_BUILD_DATE=$(date)
  fi
  echo "export CONTAINER_BUILD_DATE=\"${CONTAINER_BUILD_DATE}\"" >> $SINGULARITY_ENVIRONMENT

  # Git repository names (when building, pass via SINGULARITYENV_CONTAINER_GIT_NAME otherwise empty)
  if [ -z ${CONTAINER_GIT_NAME+x} ]
  then
    CONTAINER_GIT_NAME=''
  fi
  echo "export CONTAINER_GIT_NAME=\"${CONTAINER_GIT_NAME}\"" >> $SINGULARITY_ENVIRONMENT

  # Git repository url (when building, pass via SINGULARITYENV_CONTAINER_GIT_URL otherwise empty)
  if [ -z ${CONTAINER_GIT_URL+x} ]
  then
    CONTAINER_GIT_URL=''
  fi
  echo "export CONTAINER_GIT_URL=\"${CONTAINER_GIT_URL}\"" >> $SINGULARITY_ENVIRONMENT

  # Git repository commit id (when building, pass via SINGULARITYENV_CONTAINER_GIT_COMMIT_ID otherwise empty)
  if [ -z ${CONTAINER_GIT_COMMIT_ID+x} ]
  then
    CONTAINER_GIT_COMMIT_ID=''
  fi
  echo "export CONTAINER_GIT_COMMIT_ID=\"${CONTAINER_GIT_COMMIT_ID}\"" >> $SINGULARITY_ENVIRONMENT

  # Container version (when building, pass via SINGULARITYENV_CONTAINER_VERSION otherwise empty)
  if [ -z ${CONTAINER_VERSION+x} ]
  then
    CONTAINER_VERSION=''
  fi
  echo "export CONTAINER_VERSION=\"${CONTAINER_VERSION}\"" >> $SINGULARITY_ENVIRONMENT

  # the linux command 'make' supports the compilation of independent targets in parallel; this is also passed to R 'install.packages' since it uses 'make' internally
  COMPILE_CPUS=2

  # deactive interactive dialogs
  export DEBIAN_FRONTEND=noninteractive

  # install and reconfigure locale
  apt-get clean -q && apt-get update -y -q && apt-get upgrade -y -q
  apt-get install -y -q locales
  apt-get clean -q
  export LANGUAGE="en_US.UTF-8"
  export LANG="en_US.UTF-8"
  export LC_ALL="en_US.UTF-8"
  echo "LC_ALL=en_US.UTF-8" >> /etc/environment
  echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
  echo "LANG=en_US.UTF-8" > /etc/locale.conf
  locale-gen --purge en_US.UTF-8
  dpkg-reconfigure --frontend=noninteractive locales

  apt-get update
  apt-get install -y git cmake g++ curl

  git clone https://github.com/IEDB/TCRMatch.git -b v1.0.2
  cd /TCRMatch/ && make
  cd /TCRMatch/scripts && ./update.sh

%runscript
  /TCRMatch/tcrmatch "$@"
