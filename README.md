[![pipeline status](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-tcrmatch/badges/main/pipeline.svg)](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-tcrmatch/-/commits/main)
[![Latest Release](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-tcrmatch/-/badges/release.svg)](https://gitlab.hrz.tu-chemnitz.de/dcgc-bfx/singularity/singularity-tcrmatch/-/releases)

# Singularity Tcrmatch

containerize https://github.com/IEDB/TCRMatch
